package facci.jhonzambrano.daovi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import facci.jhonzambrano.daovi.Fragmento.frg_lista_mascota;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                Intent intent = new Intent(SplashScreen.this, Login.class);
                startActivity(intent);
                finish();

            }
    }
