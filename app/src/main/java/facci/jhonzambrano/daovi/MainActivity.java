package facci.jhonzambrano.daovi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent intent;
        switch (item.getItemId()){
            case  R.id.opcionActividad:
                intent = new Intent(MainActivity.this, GatoRegistro.class);
                startActivity(intent);
                break;
            case R.id.opcionNutricion:
                intent = new Intent(MainActivity.this, nutricion.class);
                startActivity(intent);
                break;
            case R.id.opcionFundaciones:
                intent = new Intent(MainActivity.this, fundaciones.class);
                startActivity(intent);
                break;
            case R.id.opcionContacto:
                intent = new Intent(MainActivity.this, Infomacion.class);
                startActivity(intent);
                break;
            case R.id.opcionPerfil:
                intent = new Intent(MainActivity.this, Perfil.class);
                startActivity(intent);
                break;
            case R.id.opcionCerrar:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this, Login.class));
                finish();
                break;
        }
        return true;

    }
}
